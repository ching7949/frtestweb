FROM python:3.7-rc

RUN mkdir /app && cd /app

WORKDIR /app

COPY ./FRTestWeb ./FRTestWeb

WORKDIR ./FRTestWeb

RUN pip install -r requirements.txt

CMD ["python3","manage.py", "runserver", "0.0.0.0:8000"]

EXPOSE 5000
