from django.shortcuts import render
from service.order_statistic import OrderStatistic
import datetime as dt
from django.views.decorators.cache import cache_page


@cache_page(60 * 60 * 24 * 7)
def home(request):

    ser = OrderStatistic()
    hot_pro_data = ser.get_hot_product()
    free_data = ser.get_free_shipping()
    rebuy_data = ser.get_rebuy_statistic(dt.datetime.strptime('2018-2-1', '%Y-%m-%d'), dt.datetime.strptime('2018-2-7', '%Y-%m-%d'))
    
    return render(request, 'home/home.html',
    context= {
        'hot_product': hot_pro_data,
        'free_shipping': free_data,
        'rebuy': rebuy_data
    })