import pandas as pd
import datetime as dt
from service.bq_crud import BigqueryService

class OrderStatistic:

    def __init__(self):
        self._bq_key = 'unaproj-b5476ad7f714.json'


    def get_hot_product(self):
        ser = BigqueryService(self._bq_key)

        seltop = f'''
        SELECT ROW_NUMBER() OVER(ORDER BY cnt DESC) AS ROWID,product_name,cnt
        FROM (
          SELECT COUNT(d.qty) AS cnt, d.product_name
          FROM `unaproj.test_data.order` o
          LEFT JOIN `unaproj.test_data.order_item` d ON o.order_id = d.order_id
          GROUP BY d.product_name
        ) T
        ORDER BY cnt DESC
        LIMIT 3 
        '''

        d = ser.bq_search(seltop)
        # print(d['product_name'])

        return d.sort_values(by=['ROWID']).to_json(orient='records')


    def get_free_shipping(self):
        ser = BigqueryService(self._bq_key)
        pie_data = []

        seltop = f'''
        SELECT COUNT( order_id ) AS cnt, SUM(CASE shipping WHEN 0 THEN 1 ELSE 0 END) AS FS  
        FROM `unaproj.test_data.order` 
        '''

        d = ser.bq_search(seltop)

        pie_data.append({'value': d['cnt'][0], 'name': '訂單總數'})
        pie_data.append({'value': d['FS'][0], 'name': '免運訂單數'})


        return pie_data


    def get_rebuy_statistic(self,sdt, edt):

        ser = BigqueryService(self._bq_key)
        run_dt = sdt
        daily =[]
        data=[]
        col_index = 0

        while run_dt <= edt:

            s_run_dt = run_dt.strftime('%Y-%m-%d')
            s_run_edt = (run_dt + dt.timedelta(days=1)).strftime('%Y-%m-%d')
            daily.append(s_run_dt)

            sel_sql = f"""
            WITH O AS (
              SELECT order_id, customer_id
              FROM `unaproj.test_data.order`
              WHERE dt >= '{s_run_dt}' AND dt < '{s_run_edt}'
            )
            ,P AS (
              SELECT dt, customer_id
              FROM `unaproj.test_data.order`
              WHERE customer_id IN (SELECT customer_id FROM O)
              AND dt >= '{sdt.strftime('%Y-%m-%d')}' AND dt < '{s_run_edt}'
            )
            ,R AS (
               SELECT dt, COUNT(DISTINCT customer_id) AS R_CNT
               FROM P 
               GROUP BY dt
            )
            SELECT ROUND(R_CNT/(SELECT COUNT(DISTINCT customer_id) FROM O)*100, 2) AS CNT
            FROM R
            ORDER BY dt DESC
            """

            d = ser.bq_search(sel_sql)

            row_index = 0

            for r in d['CNT']:
                data.append([col_index, row_index, r])
                row_index = row_index + 1

            col_index = col_index + 1

            run_dt = run_dt + dt.timedelta(days=1)

        x_daily = []

        for i in range(1, col_index):
            x_daily.append(f'第{str(i)}天')


        return {'daily': daily, 'x_daily': x_daily, 'data': data}