import unittest
from service.order_statistic import OrderStatistic
import datetime as dt

class TestOrderStatistic(unittest.TestCase):

    def test_get_hot_product(self):
        ser = OrderStatistic()
        result = ser.get_hot_product()
        self.assertIsInstance(result, object)

    def test_get_free_shipping(self):
        ser = OrderStatistic()
        result = ser.get_free_shipping()
        self.assertIsInstance(result, object)

    def test_get_rebuy_statistic(self):
        ser = OrderStatistic()
        result = ser.get_rebuy_statistic(dt.datetime.strptime('2018-2-1', '%Y-%m-%d'), dt.datetime.strptime('2018-2-7', '%Y-%m-%d'))
        self.assertIsInstance(result, object)


if __name__ == '__main__':
    unittest.main()